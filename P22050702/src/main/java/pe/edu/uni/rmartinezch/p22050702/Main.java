/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.uni.rmartinezch.p22050702;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author Ronald Martinez <rmartinezch@gmail.com>
 */
public class Main {

    public static void main(String[] args) throws FileNotFoundException, IOException {
        System.out.println("Files");
        // https://www.asciitable.com/
        String name = "ByteStream.txt";
        File file = new File(name);
        System.out.println("Location of file: " + file.getAbsolutePath());
        System.out.println("Writting a file");
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(82);
        fileOutputStream.write(79);
        fileOutputStream.write(78);
        fileOutputStream.write(65);
        fileOutputStream.write(76);
        fileOutputStream.write(68);
        fileOutputStream.close();

        System.out.println("Read the file");
        FileInputStream fileInputStream = new FileInputStream(file);
        int decimal;
        while ((decimal = fileInputStream.read()) != -1) {
            System.out.print((char) decimal);
        }
        System.out.println("");

        name = "CharacterStream.txt";
        file = new File(name);
        System.out.println("Location of file: " + file.getAbsolutePath());
        System.out.println("Write the file");
        FileWriter fileWriter = new FileWriter(file);
        fileWriter.write(82);
        fileWriter.write(79);
        fileWriter.write(78);
        fileWriter.write(65);
        fileWriter.write(76);
        fileWriter.write(68);
        fileWriter.close();

        System.out.println("Read the file");
        FileReader fileReader = new FileReader(file);
        while ((decimal = fileReader.read()) != -1) {
            System.out.print((char) decimal);
        }
        System.out.println("");
        
        System.out.println("List of files");
        String paths[];
        file = new File("./");
        System.out.println("Directory: " + file.getAbsolutePath());
        paths = file.list();
        for (String path : paths) {
            System.out.println(path);
        }
        
        System.out.println("Creating directories");
        String directory = "/Files/Binaries/Selected";
        String fullPath = file.getAbsolutePath() + directory;
        file = new File(fullPath);
        if (file.mkdirs()) {
            System.out.println("Directories has been created");
        } else {
            System.out.println("Directories has already been created");
        }
    }
}
